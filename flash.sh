#!/bin/bash
set -e

cargo +stable build --release

output_dir="target/thumbv7em-none-eabihf/release"
output_name="stm32l4-quickstart"

arm-none-eabi-size -Ax "$output_dir/$output_name"
arm-none-eabi-objcopy -O binary "$output_dir/$output_name" "$output_dir/$output_name.bin"
st-flash write "$output_dir/$output_name.bin" 0x8000000
