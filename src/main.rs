#![no_std]
#![no_main]

use panic_halt as _;

use core::convert::TryInto;
use cortex_m_rt::entry;
use stm32l4::stm32l4x6::Peripherals;

const CLOCK_SPEED : u32 = 12000000;
const UART_BAUD : u32 = 9600;

fn setup_clock(p : &Peripherals)
{
	let rcc = &p.RCC;

	// enable HSI
	rcc.cr.modify(|_, w| w.hsion().set_bit());
	while !rcc.cr.read().hsirdy().bit() {}

	// enable 12 MHz PLL using 16 MHz HSI: 16 / 2(PLLM) * 12(PLLN) / 8(PLLR) = 12
	// also, PLLQ (can be used for USB) will have a clock of 48 MHz: 16/2*12 / 2(PLLQ) = 48
	// however, USB will require HSE instead of HSI to be used
	rcc.pllcfgr.modify(|_, w| unsafe { w
		.pllsrc().bits(0b10) // 00: no clock, 01: MSI, 10: HSI, 11: HSE
		.pllm().bits(1) // 2
		.plln().bits(12)
		.pllr().bits(3) // 8
		.pllren().set_bit()
		// .pllq().bits(0) // 2
		// .pllqen().set_bit()
	});
	rcc.cr.modify(|_, w| w.pllon().set_bit());
	while !rcc.cr.read().pllrdy().bit() {}

	// select PLL as system clock source
	rcc.cfgr.modify(|_, w| unsafe { w.sw().bits(3) });
	while rcc.cfgr.read().sw().bits() != 3 {}

	// we don't need the MSI anymore
	rcc.cr.modify(|_, w| w.msion().clear_bit());

	// the STM32L476RG chip exposes GPIO A, B and C
	rcc.ahb2enr.modify(|_, w| w
		.gpioaen().set_bit()
		.gpioben().set_bit()
		.gpiocen().set_bit()
	);
}

fn setup_uart(p : &Peripherals)
{
	let rcc = &p.RCC;
	let gpioa = &p.GPIOA;
	let usart2 = &p.USART2;
	
	// the USART2 is connected to the stlink
	rcc.apb1enr1.modify(|_, w| w.usart2en().set_bit());
	gpioa.moder.modify(|_, w| w.moder2().alternate().moder3().alternate());
	gpioa.afrl.modify(|_, w| w.afrl2().af7().afrl3().af7());
	usart2.brr.write(|w| w.brr().bits(((CLOCK_SPEED + UART_BAUD / 2) / UART_BAUD).try_into().unwrap()));
	usart2.cr1.modify(|_, w| w.te().set_bit());
	usart2.cr1.modify(|_, w| w.ue().set_bit());
}

fn usart2_write(p : &Peripherals, c : char)
{
	let code : u8 = u32::from(c).try_into().unwrap();
	&p.USART2.tdr.write(|w| unsafe { w.tdr().bits(code.into()) });
}

#[entry]
fn main() -> ! {
	let p = Peripherals::take().unwrap();

	setup_clock(&p);
	setup_uart(&p);
	
	// set PA5 as output
	let gpioa = &p.GPIOA;
	gpioa.moder.modify(|_, w| w.moder5().output());
	
	loop {
		// blink the led
		gpioa.bsrr.write(|w| w.bs5().set_bit());
		cortex_m::asm::delay(CLOCK_SPEED / 2);
		gpioa.brr.write(|w| w.br5().set_bit());
		cortex_m::asm::delay(CLOCK_SPEED / 2);

		// write a dot to uart
		usart2_write(&p, '.');
	}
}
